\section{Descriptive analysis}
Figure \ref{fig:06} depicts the distribution of the quantitative phenotypes of the trial. The goal of this analysis is to obtain a graphical overview about the variables in order to compare the distribution and normality of the accessions. For instance, normality tests shows that only \texttt{PlantCoverage, NrTeethLeafBlade, TKM} and \texttt{DaysBranchDev} reject the hypothesis that the accessions have a normal distribution in those variables ($p$\nobreakdash-value<0.001).

Many plots share similar density curves, e.g. the number of days to flowering, milk stage, soft dough and physiological maturity have a climax in the same position of their respective plots following an alike curve, which shows a wide range of variation within the accessions. The climax emphasizes a great number of precocious accessions in each plot. More examples are the plots of plant height with petiole length, and the number of primary branches with the stem diameter.

In the overview, it is illustrated that many accessions achieved a height of approximately \SI{40}{\centi\meter} and a coverage of \SI{43}{\centi\meter}. Additionally, the accessions were characterized by their wide variation of stem diameter, grain width and dimensions of the leaf.

Figure \ref{fig:08} displays the variation of the qualitative phenotypes of the experiment. Plots such as the growth habit, color of the striae, abiotic susceptibility, leaf shape and border of the grain present heterogeneity since one category in each plot dominates within the population. For example, the \textit{Lasta} growth habit and the green striae colour in their category, respectively. In contrast, the appearance of the perigonium, the pigmentation of the steam and the axils buds are characterized by the close uniformity between the categories.

The rest of the qualitative variables show both heterogeneity and uniformity, i.e. the categories of the plots hold a similar number of accessions, still there is one category that predominates in each plot (See Figure \ref{fig:08}).

% Figure~6
\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{fig/fig008.png}
  \includegraphics[width=\textwidth]{fig/fig009.png}
  \caption{Distribution of the 18 quantitative variables of the accessions.}
  \vspace{-3mm}
  \caption*{Each plot shows the distribution of accessions regarding a variable. A peak represents a concentration of members.}
  \label{fig:06}
\end{figure}

% Perigonium is the non-reproductive part of the flower (Sepals and petals)
% Pericarp is the fruit wall (3 layer, exocarp,mesocarp,endocarp)
% Episperm -> seed coat

% Figure~8
\begin{figure}[!htb]
\centering
\includegraphics[width=\textwidth]{fig/fig010.png}
\includegraphics[width=\textwidth]{fig/fig011.png}
\caption{Distribution of the 15 qualitative variables of the accessions.}
\vspace{-2mm}
\caption*{The group \texttt{Other} in some plots contains, if available, accessions with unique phenotypes.}
\label{fig:08}
\end{figure}
\clearpage
\section{Correlations}

Figure~\ref{fig:10} shows the correlation matrix of the quantitative phenotypes. The illustration helps to compare the correlation between variables in order to discover significant relationships, which can be later linked with the hierarchical clustering of the accessions.  Correlations with significant $p$\nobreakdash-values were clustered in two groups using Ward's method. After that procedure, two large group of variables can be distinguished due to their location in the matrix, which displays the following classification:

\begin{enumerate}
  \item{\texttt{DaysFlowering, MilkStage, DoughStage, DaysPM, DaysBranchDev, \newline PlantCoverage} (See Figure~\ref{fig:12}).}
  \item{\texttt{PetioleLength, MaxLeafLength, MaxLeadWidth, SeedYieldPlant, PlantHeight, StemDiameter} (See Figure~\ref{fig:13}).}
\end{enumerate}

% Figure~10
\begin{figure}[!htb]
\centering
\includegraphics[width=\textwidth, trim=0 50 0 0]{fig/fig012.png}
\caption{Plot of the correlation matrix of the quantitative variables.}
\vspace{-2mm}
\caption*{Legend represents the coefficient of correlation~($r$). Significant correlations are marked with~$*$~($p$\nobreakdash-value<0.05); $**$~($p$\nobreakdash-value<0.01); $***$~($p$\nobreakdash-value<0.001).}
\label{fig:10}
\end{figure}

Additionally, the correlation matrix depicts only two negative significant correlations. The first between \texttt{GrainHLWeight} and \texttt{NrTeethLeafBlade} and the second between \texttt{PlantHeight} and \texttt{GrainWidth}. Their scatter-plots can be seen at the Figure~\ref{fig:14}. They are characterized by the high dispersion and heterogeneity in the trendline.

% Figure~12
\begin{figure}[!htb]
\centering
\includegraphics[width=\textwidth]{fig/fig013.png}
\vspace{-0.9cm}
\caption{Scatter-plots of the first group of quantitative variables by Ward's method.}
\vspace{-1.5mm}
\caption*{The top-right side of the plot illustrates the correlation coefficients ($r$). Significant correlations are marked with~$*$~($p$\nobreakdash-value<0.05); $**$~($p$\nobreakdash-value<0.01); $***$~($p$\nobreakdash-value<0.001).}
\label{fig:12}
\end{figure}

Scatter-plots show the relationship between two variables. They are useful to distinguish and link group of phenotypes to their respective cluster and characterize them. Figure~\ref{fig:12} illustrates the first group of correlations with their respective coefficients and scatter-plots. This cluster is characterized by their high significance, especially between the variables \texttt{DaysFlowering, MilkStage, DoughStage} and \texttt{DaysPM}, whose positive linear correlations are almost perfect. The correlations with \texttt{DaysBranchDev} and \texttt{PlantCoverage} share the characteristic that their scatter-plots contain a trend that is somewhat parallel to the x-axis.
% The variable \texttt{DaysBranchDev} shares in this cluster only two significant correlations with a positive trend, namely with \texttt{DaysFlowering} ($r$=\SI{0.40}) and \texttt{MilkStage} ($r$=\SI{0.29}). Furthermore, \texttt{PlantCoverage} shares a significant correlation with every members of the group except \texttt{DaysBranchDev}. However, these positive correlations range only between \SIrange{0.32}{0.35}.

% Figure~13
\begin{figure}[!ht]
\centering
\includegraphics[width=\textwidth]{fig/fig014.png}
\vspace{-0.9cm}
\caption{Scatter-plots of the second group of the quantitative variables by Ward's method.}
\vspace{-1.7mm}
\caption*{The top-right side of the plot illustrates the correlation coefficients ($r$). Significant correlations are marked with~$*$~($p$\nobreakdash-value<0.05); $**$~($p$\nobreakdash-value<0.01); $***$~($p$\nobreakdash-value<0.001).}
\label{fig:13}
\end{figure}
\pagebreak
Figure~\ref{fig:13} illustrates the second group of significant variables from the matrix correlation. All coefficients have positive values. Unlike the first group (See Figure~\ref{fig:12}), the scatter-plots do not exhibit almost perfect positive linear correlations. However, there is a pattern recognizable in the plots with \texttt{PetioleLength, MaxLeafLength, MaxLeafWidth} and \texttt{StemDiameter} that displays a somewhat positive correlation trendline. In contrast, \texttt{SeedYieldPlant} and \texttt{PlantHeight} show plots, whose trendlines are to some extent parallel to the x-axis.
% \texttt{MaxLeafLength} and \texttt{MaxLeafWidth} are the only combination in this cluster that represents a high positive coefficient. ($r$=\SI{0.91}). The rest of the coefficient fall between \SIrange{0.30}{0.55}. Also, combinations with \texttt{StemDiameter} shows coefficient values hovering around \SI{0.50}. This signifies a positive trends with high significance ($p$\nobreakdash-value<0.001). \texttt{PlantHeight} highlights two non-significant results with \texttt{PetioleLength} ($r$=\SI{0.16}) and \texttt{SeedYieldPlant} ($r$=\SI{0.24}).
% Figure~14
\begin{figure}[!htb]
\centering
\includegraphics[width=0.73\textwidth]{fig/fig022.png}
\vspace{-0.65cm}
\caption{Scatter-plots of the negative quantitative correlations from the correlation matrix.}
\vspace{-1mm}
\caption*{The top-right side of the plot illustrates the correlation coefficients ($r$). Significant correlations are marked with~$*$~($p$\nobreakdash-value<0.05); $**$~($p$\nobreakdash-value<0.01); $***$~($p$\nobreakdash-value<0.001).}
\label{fig:14}
\end{figure}

% In Figure~\ref{fig:14} there are only two positive correlation, namely between \texttt{GrainHLWeight} and \texttt{PlantHeight}, and the correlation of \texttt{NrTeethLeafBlade} and \texttt{GrainWidth}. Nonetheless, these correlations (\SI{0.16} and \SI{0.22}) are not significant. All the negative coefficient ranged between \SIrange{-0.17}{-0.28}.
\clearpage
\section{Principal component analysis}\label{sec:pca}

The PCA is a tool, which helps recognize the relationship and correlations between variables and each member of a population. Accessions near a variable will be most likely be represented by that specific trait. The plot of Figure~\ref{fig:15} demonstrates the first two dimensions (Dim), which explains the variability of the data. In total, both dimensions are able to explain 43.5\% of the variation from the quantitative traits.
% \texttt{Dim2} explains 17.3\% while \texttt{Dim1} explains 26.2\% of the variability of the quantitative data. 
% Figure~15
\begin{figure}[!htb]
  % [!htb]
  \centering
  \includegraphics[width=\textwidth]{fig/fig015.png}
  \caption{PCA bi-plot.}
  \vspace{-1mm}
  \caption*{Plot of all accessions and quantitative variables based on the first two principal components. The values of each dimension show how much variation can be explained by the dimension. The clustering was based on Ward's method. Black dots represents the qualitative variable, while colored dots, triangles or squares represent a member of the sample based on the their cluster.}
  \label{fig:15}
  \end{figure}

The quantitative variables can be distributed across groups based on their relationship and correlation with each other. A group is defined by adjacent variables to each other. The result are the following four groups:
\begin{enumerate}
  \item \texttt{MaxLeafWidth, MaxLeafLength, StemDiameter, PetioleLength, SeedYieldPlant, PlantHeight}\label{itm:group1}
  \item \texttt{GrainHLWeight, TKM, GrainWidth}\label{itm:group2}
  \item \texttt{HI, NrPrimaryBranch, NrTeethLeafBlade, PlantCoverage, DaysBranchDev}\label{itm:group3}
  \item \texttt{MilkStage, DaysPM, DaysFlowering, DoughStage}\label{itm:group4}
\end{enumerate}

Group \ref{itm:group1} is characterized for their contribution towards \texttt{Dim2} despite their partial input to \texttt{Dim1}. Group \ref{itm:group4} contributes more to \texttt{Dim1} than the other variables. The variables of this group share very similar projections, as they are in close proximity in the plot. Group \ref{itm:group3} do not play or has a very small role in \texttt{Dim2} but contributes to some extent to \texttt{Dim1}. In contrast, the general position of group \ref{itm:group2} is almost at the centre of the plot.
% with a negative trend observed in \texttt{Dim1}. \texttt{GrainHLWeight}, however, exhibits a positive trend in \texttt{Dim2}.

The accessions were clustered in 3 groups based on Ward's method (See \nameref{sec:hcpc}). Cluster \texttt{A} dominates the left axis of the PCA bi-plot including the variable group \ref{itm:group3}, and partly \ref{itm:group1} and \ref{itm:group4}. Meanwhile, Cluster \texttt{B} extends more to the right axis. This cluster includes variables from group \ref{itm:group2} and \ref{itm:group3} and partly \ref{itm:group1}. Cluster \ref{itm:group3} has only one member and therefore, has no ellipsis in the plot but the single dot is located in the bottom-right quadrant, which is also nearer to cluster \ref{itm:group4}.

\section{Multiple correspondence analysis}\label{sec:mca}

The MCA depicts the relationships between phenotypes and each accession from the population. Accessions near a phenotype will be most likely be represented by that specific characteristic. Figure~\ref{fig:16} shows the first two dimensions that explain the most about the variability of the qualitative variables. These components can only justify 13.6\% of the total variation of the population.

Due to the vast number of qualitative variables and their levels (with a total of 90 levels), it is impossible to create a comprehensible plot including all levels for each factor. Thus, Figure~\ref{fig:16} only captures 25 levels with the highest $\cos^2$ values, which highlights the importance of its component. In appendix \ref{appendix:01}, explanations of the qualitative variables and their corresponding levels are available. 
% Figure~16
\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{fig/fig016.png}
  \caption{MCA bi-plot.}
  \vspace{-2mm}
  \caption*{Plot of all accessions and the most important categories from the qualitative variables based on the first two principal components. The values of each dimension show how much variation can be explained by the dimension. The clustering was based on Ward's method. Black dots represents phenotypes while colored dots represent a member of the sample based on the their cluster.}
  \label{fig:16}
\end{figure}

In Figure~\ref{fig:16}, the 25 levels cannot be easily distinguished in groups. Thus, it is assumed for simplicity that each category belonged to a group based on their location. For their classification, each quadrant of the plot was considered a group. Each group represents its quadrant number, respectively.

Group 1 is characterized by the dispersion of its members in the quadrant (I) and its tendency towards both dimensions. Moreover, \texttt{Pampalasta} is a phenotype with a significant importance to \texttt{Dim2}. Group 2, 3 and 4 do not play a big role in \texttt{Dim2}, as they are located along the horizontal axis. However. their members show a wide dispersion of their projections for \texttt{Dim1}.

The accessions were clustered in 3 groups based on Ward's method (See \nameref{sec:hcpc}). Cluster \texttt{A} is located around the centre of the plot and have a small tendency to positives projections in \texttt{Dim1}. Similarly, this cluster includes all phenotypes of quadrant IV, III and partly II. In contrast, cluster \texttt{B} shows a trend toward negatives projections in \texttt{Dim1}, this includes the phenotypes from quadrant II, III and partly IV. Cluster \texttt{C} consists of only one accession and is located in the centre of quadrant I.

\section{Factor analysis of mixed data}

The main goal of this procedure, like the PCA and MCA, is to distinguish groups of variables based on their proximity, correlation and relationships with the accessions within all plots. Accessions and their respective clusters, (See Figure \ref{fig:17}) which are relatively closer to a phenotype or quantitative trait, are most likely to represented by them.

Figure \ref{fig:18} shows the contribution of the quantitative variables (\ref{fig:18a}) and the most important phenotypes (\ref{fig:18b}). As with the MCA (Section~\ref{sec:mca}), due to the amount of levels, only 25 phenotypes are plotted, which contribute with most of the explanation of the dimensions. Figure \ref{fig:17} show the loading plot of the FAMD. The clustering was based on Ward's method. Additionally, all plots share the same dimensions and explain a total of 15.3\% of the variability of the population.
% Figure~17
\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{fig/fig017.png}
  \caption{FAMD plot of the accessions.}
  \vspace{-1mm}
  \caption*{Plot of all accessions based on the clustering by Ward. The values of each dimension show how much variation can be explained by the dimension. Each number and dot represents an accession from the population.}
  \label{fig:17}
\end{figure}
% Figure~18
\begin{sidewaysfigure}
\centering
\begin{minipage}[b]{.5\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/fig018.png}
\subcaption{Quantitative variables}
\label{fig:18a}
\end{minipage}%
\begin{minipage}[b]{.5\linewidth}
\centering
\includegraphics[width=\textwidth]{fig/fig019.png}
\subcaption{Most influential phenotypes}
\label{fig:18b}
\end{minipage}%
\caption{FAMD plot of the phenotypes.}
\caption*{Plot of all quantitative variables and the most important phenotypes based on the first two principal components. The values of each dimension show how much variation can be explained by the dimension. Black triangles represents the variables and phenotypes, respectively.}
\label{fig:18}
\end{sidewaysfigure}

For simplicity, in both plots (Figure \ref{fig:18}) it is assumed that each variable and phenotype belonged to a group based on their location in their respective plots. Like with the MCA, each quadrant of each plot was considered a group. Each group represents its quadrant number for the plot, respectively. In some cases, a cluster of variables or phenotypes can seen due to their proximity within members. e.g in Figure \ref{fig:18a}, \texttt{DaysPM, DaysFlowering, DoughStage} and \texttt{MilkStage} are adjacent to each other or in Figure \ref{fig:18b}, \texttt{Perigonium\char`_Light cream, Axils\char`_FALSE, Episperm\char`_Yellowish Brown} and \texttt{AbioticStress\char`_Low} are close.

Figure \ref{fig:17} depicts the loading plot of the FAMD. All accessions were grouped in 3 clusters based on Ward's method (See \nameref{sec:hcpc}). Cluster \texttt{A} dominates in the right axis. On the other hand, cluster \texttt{B} dominates in the left axis. Additionally, this cluster shows a trend to to negative projections in \texttt{Dim2}. Cluster \texttt{C} has only one member \texttt{[2]}. This accession is located in the quadrant I and play a big role in both dimensions.
\clearpage

\section{Hierarchical clustering}\label{sec:hcpc}
The hierarchical clustering illustrates the relationships between accessions. This technique uses the information of the FAMD and proceeds with the classification of the clusters using Ward's criterion, which is the total within-cluster variance. The output of this procedure is shown in Figure \ref{fig:19}. Fifty-six accessions were categorized in 3 clusters on the basis of the FAMD. Cluster \texttt{A} has 27 members, cluster \texttt{B} contains 28 accessions and cluster \texttt{C} is defined only by accession \texttt{[2]}. 
% Figure~19
\begin{figure}[!htb]
\centering
\includegraphics[width=\textwidth]{fig/fig020.png}
\vspace{-1.2cm}
\caption{Cluster dendrogram.}
\vspace{-1mm}
\caption*{Relationship between the 56 \textit{C. pallidicaule} plants. Each number represent a accession. Grey striped-lines cover the structure of a cluster. The y-axis represents the value of Ward's criterion. A high intersection between two accession shows a higher dissimilarity.}
\label{fig:19}
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{fig/fig024.png}
  \caption{Phenotypic characterization of the clusters.}
  \vspace{-1mm}
  \caption*{Each plot illustrates the characteristics of each cluster. Each plot shows the original name of a variable. The group \texttt{Other} in some plots contains, if available, accessions with unique phenotypes.}
  \label{fig:21}
  \end{figure}

In order to compare the traits of the clusters, means ($\mu$) and standard deviation ($\sigma$) of the quantitative variables of each cluster were calculated. Table \ref{tab:03} shows their results. A lower $\sigma$ value relative to its variable demonstrates uniformity in the cluster.

Additionally, the phenotypes of the clusters were plotted in bar charts (See Figure \ref{fig:21}). The Figure shows that there are some phenotypes that dominate in all clusters like \textit{Lasta} growth habit and oval leaf shape; phenotypes are present in only one cluster e.g. \texttt{Yellow} in \texttt{PigmentedAxilColour}; and many variables are characterized by their variation of phenotypes within the clusters. 

\begin{table}[!htb]
  \centering
  \caption{Means and standard deviation of the clusters.}
  \label{tab:03}
  \resizebox{0.8\textwidth}{!}{%
  \begin{tabular}{ll|rrr|c@{}@{}l@{}@{}c}
    \toprule
  Variables &  &  & $\mu$ &  &  & $\sigma$ &  \\
   &  & \texttt{A} & \texttt{B} & \texttt{C} & \texttt{A} &  & \texttt{B} \\ \hline
  \texttt{PlantHeight} & [\SI{}{\centi\meter}] & 38.40 & 42.99 & 25.00 & 8.17 &  & 4.58 \\
  \texttt{StemDiameter} & [\SI{}{\centi\meter}] & 4.57 & 4.94 & 3.85 & 0.68 &  & 0.67 \\
  \texttt{NrPrimaryBranch} & [$n$] & 8.82 & 8.48 & 9.00 & 1.81 &  & 1.58 \\
  \texttt{PlantCoverage} & [\SI{}{\centi\meter}] & 37.77 & 39.53 & 77.50 & 6.23 &  & 6.88 \\
  \texttt{NrTeethLeafBlade} & [$n$] & 3.33 & 3.33 & 6.80 & 0.72 &  & 0.56 \\
  \texttt{PetioleLength} & [\SI{}{\centi\meter}] & 1.64 & 1.74 & 1.55 & 0.24 &  & 0.14 \\
  \texttt{MaxLeafLength} & [\SI{}{\centi\meter}] & 2.72 & 3.02 & 2.58 & 0.26 &  & 0.19 \\
  \texttt{MaxLeafWidth} & [\SI{}{\centi\meter}] & 2.64 & 2.90 & 2.41 & 0.30 &  & 0.21 \\
  \texttt{GrainWidth} & [\SI{}{\milli\meter}] & 1.17 & 1.13 & 1.34 & 0.06 &  & 0.08 \\
  \texttt{TKM} & [\SI{}{\gram}] & 0.92 & 0.83 & 1.10 & 0.19 &  & 0.16 \\
  \texttt{GrainHLWeight} & [\SI{}{\gram\per\cubic\centi\metre}] & 0.67 & 0.67 & 0.61 & 0.03 &  & 0.02 \\
  \texttt{SeedYieldPlant} & [\SI{}{\gram}] & 9.46 & 10.11 & 14.34 & 3.91 &  & 3.80 \\
  \texttt{DaysBranchDev} & [$d$] & 24.89 & 25.85 & 30.00 & 1.66 &  & 2.09 \\
  \texttt{DaysFlowering} & [$d$] & 57.14 & 63.74 & 74.00 & 4.00 &  & 4.97 \\
  \texttt{MilkStage} & [$d$] & 79.64 & 87.37 & 99.00 & 4.13 &  & 5.30 \\
  \texttt{DoughStage} & [$d$] & 95.61 & 104.67 & 119.00 & 5.20 &  & 6.22 \\
  \texttt{DaysPM} & [$d$] & 128.82 & 139.00 & 155.00 & 6.12 &  & 7.27 \\
  \texttt{HI} &  & 37.80 & 39.28 & 31.16 & 7.42 &  & 5.63\\\bottomrule
  \end{tabular}%
  }
  \vspace{2mm}
  \caption*{Clustering of the accessions based on Wards' method. \texttt{HI} is an index with a range of 0 to 100. $\mu$ represents the mean of the cluster, $\sigma$ is the standard deviation, $d$ is defined as the number of days.}
\end{table}
