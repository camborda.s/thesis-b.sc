% Figures down, tables UP
\section{Presenting the crop}

Ca\~nihua (\textit{Chenopodium pallidicaule} Aellen), also known as, ka\~nihua, ca\~nahua or ka\~nahua is a crop of the Amaranthaceae family originally from the high regions of the Altiplano, between the borders of Bolivia and Per\'u.

Ca\~nihua belongs to the genus \textit{Chenopodium} including other crops such as quinoa (\textit{Chenopodium quinoa}) and kiwicha (\textit{Chenopodium caudatus}), which today is highly regarded in the local market in the Andes.

Historically, it is believed that after 600 A.D., families from the Andean civilization Tiahuanaco started the cultivation of ca\~nihua near the lake Titicaca. However, there is a lack of archaeological evidence supporting this statement. Ca\~nihua was used by the Inca empire for generations~\citep{gade1970}.
Moreover, it is known that during the Spanish colonial period in South America, Haciendas in the Altiplano had large plantations of ca\~nihua, principally for the nourishment of the farmers~\citep{variabilidad2012}. Nevertheless, its cultivation was relegated over time, almost leading to its extinction~\citepalias[2005]{ipgri2005}.

Ca\~nihua is mainly cultivated in the Altiplano plateau. There, the crop has adapted to the high altitudes and over time has developed a cold tolerance, which helps to facilitate food security for these high-risk regions~\citepalias[2005]{ipgri2005}. In the Altiplano region the genetic and morphological variability of ca\~nihua is the highest and therefore, is expected to be the center of origin of the plant~\citep{manejo2010}.

\pagebreak
% figure 1
\begin{figure}[t]
\includegraphics{fig/fig001.jpg}
\caption{\textit{Chenopodium pallidicaule} Aellen}
\vspace*{-3mm}
\caption*{Source:~\cite{crops2007}}
\label{fig:01}
\end{figure}

\section{Botanical description}\label{sec:bot}
It was not until 1929 when Swiss botanist Paul Aellen classified ca\~nihua as a different species from quinoa~\citep{tapia2000}. Long before that, it was assumed that ca\~nihua was a variety of quinoa~\citep{gade1970}.

Ca\~nihua is a herbaceous annual plant which can grow as high as \SI{70}{\centi\meter}~\citep{fao1995} with a vegetative period that lasts for around 150 days~\citep{manejo2010}.

There exist three types of growth habits of ca\~nihua (See Figure \ref{fig:02}): Saihua, which has upright stems with few secondary branches; Lasta, whose stems cover a wider area in the air; Pampalasta, whose stems grow almost parallel to the surface~(\citeauthor[2010]{manejo2010}; \citet{fao1995}).
% Figure 2
\begin{figure}
\centering
\begin{minipage}[b]{.3\linewidth}
\centering
\includegraphics[viewport=30 40 140 300]{fig/fig002.png}
\subcaption{Saihua}
\end{minipage}%
\begin{minipage}[b]{.3\linewidth}
\centering
\includegraphics{fig/fig003.png}
\subcaption{Lasta}
\end{minipage}
\begin{minipage}[b]{.3\linewidth}
\centering
\includegraphics{fig/fig004.png}
\subcaption{Pampalasta}
\end{minipage}
\caption{Growth habits of ca\~nihua}
\vspace*{-1.5mm}
\caption*{Source:~\citetalias[2005]{ipgri2005}}
\label{fig:02}
\end{figure}

Ca\~nihua's primary root is a taproot, which grows between \SIrange{13}{16}{\centi\meter} and varies from white to pale pink~\citep{manejo2010}. In this crop, lateral roots are thin and numerous~(\citeauthor[2010]{manejo2010}; \citet{fao1995}).
\pagebreak

The hollow plant's main stem reaches a diameter of \SIrange{3.5}{4}{\milli\meter} at its physiological maturity. Ramifications begin from the base of the plant and its number changes depending on the ecotype; approximately from 11 to 16 branches per plant~\citep{manejo2010}. The colour of the stem plant is determined by the ecotype and corresponding phenological stage, i.e. it shows a wide spectrum of colour variation from yellow up to dark purple~(\citeauthor[2010]{manejo2010};~\citet{fao1995}).

The colour of the plant leaves also changes depending on the phenological stage and ecotype of the plant~\citep{fao1995}. The petiole of ca\~nihua are short, ranging from \SIrange{10}{12}{\milli\meter}. The leaf arrangement is alternated, the leaf blade shape can be rhomboidal, triangular or oval~\citep{tapia2000}, has a width that can range from \SIrange{2}{2.8}{\centi\meter} and length from \SIrange{3}{5}{\centi\meter}. Furthermore, the leaves have three main distinguishable veins~\citep{manejo2010}.

Ca\~nihua shows a dense cluster of small apetalous flowers, also known as glomerule at the end of the stock~\citep{fao1995}. These glomerules are covered normally by the leaves and are developed from the axillary buds~(\citeauthor[2010]{manejo2010}; \citet{fao1995}). Its flowers have three variations of their reproductive morphology: hermaphrodites, male sterile and pistillate. Normally, the androecium consists of one to three stamina while the gynoecium has only one locule~(\citeauthor[2010]{manejo2010}; \citet{fao1995}).
\cite{mujica2002} (as cited in~\citealt{nunez2011}) reports that Ca\~nihua has a self-pollination rate between 64\% to 89\% and a cross-pollination rate of 20\% to 46\%.
\pagebreak
Ca\~nihua is considered a pseudo-cereal since they share characteristics with cereals like dry seeds with high amounts of starch~\citep{rosentrater2017}. Moreover, ca\~nihua's fruit are achene~\citep{tapia2000}. The grains have a diameter of 1.0 to 1.2~mm and their shape varies depending on the ecotype: they may have a nearly cylindrical, conical or nearly elliptical shape~(\citeauthor[2010]{manejo2010}; \citetalias[2005]{ipgri2005}). The seeds show no dormancy at all and may germinate in the plant if conditions are met~\citep{fao1995}.

\section{Nutritional value}

When comparing ca\~nihua with other crops, a higher protein content is normally expected. Their composition can be seen in Table~\ref{tab:compo}. Nonetheless, \cite{repo2003} explains in their research that the protein content and its quality depends on the variety used. The lack of animal protein in the Altiplano can be overcome by utilizing Andean crops to fulfill the need of a protein source~\citep{tapia2000}.

Starch is the major source of carbohydrate content of ca\~nihua, similar to what is obtainable in other grains. In addition, ca\~nihua has a higher sugar composition than other Andean crops. Likewise, \cite{repo2003} explains that Andean crops such as ca\~nihua may be used for oil extraction. For instance, ca\~nihua oil yield have an average of 6.4\% and it is rich with unsaturated fatty acids.

\begin{table}[h]
\centering
\caption{Composition of cereals and Andean grains (g/100 g dry matter).
}
\label{tab:compo}
\resizebox{.85\textwidth}{!}{%
\begin{tabular}{@{}l@{\hspace{0.6in}}ccccc@{\hspace{0.1in}}}
\toprule
 & Protein & Fat & Raw fiber & Ash & Carbohydrate \\ \midrule
Quinoa & 14.4 & 6.0 & 4.0 & 2.9 & 72.6 \\
Ca\~nihua & 18.8 & 7.6 & 6.1 & 4.1 & 63.4 \\
Kiwicha & 14.5 & 6.4 & 5.0 & 2.6 & 71.5 \\
Rice & 9.1 & 2.2 & 10.2 & 7.2 & 71.2 \\
Common rye & 13.4 & 1.8 & 2.6 & 2.1 & 80.1 \\
Corn & 11.1 & 4.9 & 2.1 & 1.7 & 80.2 \\
Triticale & 15.0 & 1.7 & 2.6 & 2.0 & 78.7 \\
Barley & 11.8 & 1.8 & 5.3 & 3.1 & 78.1 \\
English Wheat & 10.5 & 2.6 & 2.5 & 1.8 & 78.6 \\ \bottomrule
\end{tabular}%
}
\vspace*{1mm}
\caption*{Source:~\cite{Kent1983,repo1992} cited by~\cite{repo2003}}
\end{table}
\vspace*{-6mm}
\section{The Altiplano}

The Altiplano is a plateau in South America located in the southwest of Peru and western Bolivia. It also extends partly through the north of Chile and Argentina (See Figure~\ref{fig:05}). The plateau has an average height of \SI{3,650}{\meter} above sea level~\citep{brit2011}.

% Figure 5
\begin{figure}[!htb]
\centering
\includegraphics[width=0.86\textwidth]{fig/fig007.png}
\vspace{-3mm}
\caption{Proximate location of the Altiplano}
\label{fig:05}
\end{figure}

The climate of the plateau is affected by the high altitudes. Annually, the average temperatures may vary between \SIrange{3}{12}{\degreeCelsius} and the average precipitation ranges from approximately \SIrange{200}{800}{\milli\meter} in the humid areas, i.e the northeastern region. The southwestern region of the Altiplano is the coldest and driest area of the plateau~\citep{pariona2011}.

Additionally, the vegetation is dominated by grasses and shrubs because of its arid conditions. The altitude, climate and high saline content makes it difficult for trees to grow. Some of the common species in this region are \textit{Azorella~compacta}, \textit{Jarava~ichu}, and \textit{Festuca~dolichophylla}~\citep{pariona2011}.

Maize (\textit{Zea~mais}) and wheat (\textit{Triticum~aestivum}) can be grown in the surroundings of the Lake Titicaca due to the moderate temperatures from that area despite the high altitude of \SI{3,900}{\meter}. The most important cities in the Peruvian Altiplano are Puno and Juliaca~\citep{brit2011}. Ca\~nihua production in Puno reaches approximately \SI{46}{\deci\tonne} annually spanning an area of \SI{5,600}{\hectare}~\citep{carcausto2018}.