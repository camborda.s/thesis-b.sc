\section{Materials}

In this research the information of 56 wild \textit{C.~pallidicaule} accessions, gathered in Peru by the agronomist Roxana Mar\'on Coaquira, was utilized. The data contained information about characterization and evaluation descriptors for each accession, based on \citetalias{ipgri2005} (2005). 

The original field experiment was located in the Department of Puno at the coordinates \ang{15;41;58}S~\ang{70;04;07}W and the accessions were sowed between the \nth{26} of December 2015 and the \nth{16} of January 2016. The trial had a circular design and consisted of 56 sets of ca\~nihua accessions (See Figure \ref{fig:22}). There were no replicates and the choice of the design was to avoid the water logging.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth, trim=340 173 340 180,clip]{fig/fig025.png}
  \caption{Aerial view of the field experiment.}
  \vspace{-0.3cm}
  \caption*{Each block represents an accession. The photo was taken prior physiological maturity by a drone at a height of \SI{50}{\meter}. Photographer: Roxana Mar\'on Coaquira.}
  \label{fig:22}
\end{figure}

Data about the temperatures and precipitation were obtained from the Illpa weather station on in Puno at \ang{15;41;}S~\ang{70;05;}W.

Characterizations descriptors show general phenotypic traits that are simple to differentiate between accessions. Normally, these traits show high heritability rates. Twenty-six traits were taken into consideration

\begin{multicols}{3}
\begin{enumerate}
  \item Growth habit
  \item Plant height
  \item Steam diameter
  \item Striae colour
  \item Stem colour
  \item Presence of pigmented axils
  \item Axil colour
  \item Number of primary \newline branches
  \item Plant coverage
  \item Leaf shape
  \item Number of teeth in the leaf blade
  \item Petiole length
  \item Maximal leaf length
  \item Maximal leaf width
  \item Leaf colour
  \item Dehiscence degree
  \item Perigonium appearance
  \item Perigonium colour
  \item Grain width
  \item Grain hectolitre weight
  \item Thousand kernel weight
  \item Seed yield
  \item Pericarp colour
  \item Episperm colour
  \item Grain shape
  \item Grain border
\end{enumerate}
\end{multicols}

Evaluation descriptors are traits that are highly susceptible to the environment and thus, special measurement tools are needed to calculate their values. The data of ten variables was used for the analyses.

\begin{multicols}{3}
\begin{enumerate}
  \item Days to branch development
  \item Days to start of flowering
  \item Days to milk stage
  \item Days to dough stage
  \item Days to physiological maturity
  \item Abiotic stress susceptibility
  \item Biotic stress susceptibility
    \begin{enumerate}
    \item Pests
    \item Diseases
    \end{enumerate}
  \item Harvest index
\end{enumerate}
\end{multicols}

In total, by merging both descriptors and taking uniformities off the analysis, 15 qualitative variables and 18 quantitative variables were considered for the experiment. Appendix \ref{appendix:01} shows the names of the variables used for the multivariate analysis and their corresponding description and formulas. They are based on the book \textit{Descriptores para ca\~nahua (Chenopodium pallidicaule Aellen)} by~\citetalias{ipgri2005} (2005). 

\section{Statistical Analysis}
The data of the plants was imported from a MS~Excel (.xlsx) document into a Rmarkdown (.rmd) file, which was utilized in the integrated development environment (IDE) Rstudio provided with R~3.5.3 and then transformed into data frames for the statistical multivariate analysis and their respective representations. Data and code is available at \url{http://server228vsl.ipsp.uni-hohenheim.de/kjschmid/stefano-camborda-bachelor-thesis-canihua.git}

For the analysis of the quantitative traits, three methods were applied:
\begin{description}
  \item[Descriptive analysis]
  The distribution of the traits was plotted using the package \texttt{ggplot2}~\citep{ggplot2} for an optimal visualization of the data. Means and ranges of the data were placed in tables as well.

  The variables were also tested for their normality using the Shapiro-Wilk test with the equation:
  \begin{equation}
    \label{eq:03}
    W= \frac{(\sum_{i=1}^na_{i}x_{(i)})^2}{\sum_{i=1}^n(x_i-\overline{x})^2}
  \end{equation}
  with $x_{(i)}$ as the ordered values of the sample and $a_i$ as tabulated coefficients.
  
  \item[Correlation]
  The correlation of the variables was calculated using the package \texttt{Hmisc}~\citep{hmisc} based on the Pearson correlation with the formula:
  \begin{equation}
  \label{eq:00}
  r=\frac{\sum(x - m_x)(y - m_y)}{\sqrt{\sum(x - m_x)^2 \sum(y - m_y)^2}}
  \end{equation}
  With $m_{x,y}$ as means of variables $x$ and $y$.
  
  The $p$-values were determined using the same package and the correlation matrix plot was designed with the package \texttt{corrplot} \citep{corrplot2017}. Scatter-plots were created using a script modified from a function of the \texttt{PerformanceAnalytics} package \citep{corre2018}.

  \item[Principal component analysis]
  The objective of the Principal component analysis~(PCA) is to compress the number of components of data-set by selecting the variables with more information. The basic equation of the PCA is:
  \begin{equation}
  % equ.1
  \label{eq:01}
  Y=W'X
  \end{equation}
  where $X$ is a matrix with $i$ observations and $j$ variables; $W$ a matrix with the eigenvectors of the co-variance matrix $S$ with the equation:
  % equ2
  \begin{equation}
  \label{eq:02}
  S_{ij}= \frac{\sum\limits_{k=1}^{n}(x_{ik}-\overline{x}_i)(x_{jk}-\overline{x}_j)}{n-1}
  \end{equation}
  
  For this procedure, the package \texttt{FactoMineR} created by \cite{facto2008} was employed.
\end{description}

For the analysis of the qualitative variables, two methods were utilized:
\begin{description}
  \item[Descriptive analysis]
  Additionally, with the \texttt{ggplot} package, frequencies of the different traits were plotted to show the range of variation of the plants.
  \item[Multiple correspondence analysis]
  This analysis is derived from the correspondence analysis (CA) and shows pattern of relationships of nominal data. As with the CA, the multiple correspondence analysis (MCA) shows two set of scores: one for the rows and one for the columns, respectively. 
  
  They are derived from a singular value decomposition of $X$ and have the following equations:
  % Equ.3
  \begin{equation}
  \label{eq:3}
  F=D^\frac{1}{2}_r P\Delta
  \end{equation}
  
  with $D_r$ as diagonal matrix of vector $r$ (vector of total rows), and $P\Delta$ as matrix of the left singular vectors multiplied by the diagonal matrix of singular values.
  % Eq.4
  \begin{equation}
  \label{eq:4}
  G=D^\frac{1}{2}_c Q\Delta
  \end{equation}
  with $D_c$ as diagonal matrix of vector $c$ (vector of total columns), and $Q\Delta$ as matrix of the right singular vectors multiplied by the diagonal matrix of singular values.
  
  The idea of the MCA is to take the nominal values of a matrix and convert them into values ranging from \SIrange{0}{1}. After that, the data is corrected and the cosine and contribution values of the transformed data-set are calculated. The MCA was calculated with the \texttt{FactoMineR} package.
  
\end{description}

To analyse quantitative and qualitative variables, two procedures were utilized:
\begin{description}
  \item[Factor analysis of mixed data]
  This analysis (FAMD) is a combination of PCA and MCA. Quantitative variables are standardized while qualitative variables are transformed into quantitative variables like in the MCA. The package \texttt{FactoMineR} handles FAMD as well.
  \item[Hierarchical clustering on principal components]
  With the information of the FAMD, the distance between individuals can be measured and then plot into a dendrogram (by means of a hierarchical clustering) based on Ward's minimum variance method. The number of clusters is derived from a partition comparison. After that, K-means clustering is run to improve the initial partition.
\end{description}
